# Role-Based Access Control (RBAC) in a Web Application

A basic web application implementing Role-Based Access Control (RBAC) to demonstrate access control mechanisms and security configurations.

## Tools and Technologies

- HTML, CSS, JavaScript 
- Node.js with Express framework
- MySql datbase

